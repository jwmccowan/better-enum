type ObjectValues<T> = T[keyof T];

const CHARACTER_STATS = {
    STRENGTH: "str",
    DEXTERITY: "dex",
    WILL: "will"
} as const;

type CharacterStats = ObjectValues<typeof CHARACTER_STATS>;

