# "Better" enums

This is my quick example of "better" `enum`s.

It is based on [Matthew Pocock's great video on enums](https://www.youtube.com/watch?v=jjMbPt_H3RQ)

Check `index.ts` for the copy/paste-able code.

## Things to note:

Obviously you can't just copy/paste and be done.  Names need to change etc., but it's important to keep the following in mind when modifying:
1. do not remove the `as const` at the end of the object definition.  This is the secret sauce for the whole thing.
2. enums are usually in UPPER_SNAKE_CASE (I forget what that is actually called)
3. types are usually in PascalCase
4. you *can* name the enum and type the same, but I strongly recommend against it.  It can mess up language servers and click throughs.
5. the `ObjectValues<T>` type util doesn't need to be defined local to the enum, go ahead and place it in type utils directory

## What's wrong with ts enums?
 [TODO]
